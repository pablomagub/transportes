<?php

namespace calculadoraBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
      //http://stackoverflow.com/questions/1385801/how-can-i-get-the-day-of-a-specific-date-with-php

        $month=date('n');//mes actual
        $year=date('Y');//año actual
        $numero = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $numero_anterior = cal_days_in_month(CAL_GREGORIAN, $month-1, $year);

        $timestamp_dia_primero = strtotime($year."-".$month."-1");
        $timestamp_diaActual = strtotime($hoy = date("Y-m-d"));
        //dia de la semana (0-6)
        $d_semana =date('w', $timestamp_dia_primero);//dia semana primer dia

        $mes=array();
        $semana=array();

        for ($i=0; $i < $d_semana; $i++) {
          if(sizeOf($semana)>6){
              array_push($mes,$semana);
              $semana=array();
          }else {
            array_push($semana,($numero_anterior-$d_semana+1)+$i);
          }
        }
        for ($i=1; $i <= $numero; $i++) {
          if(sizeOf($semana)>6){
              array_push($mes,$semana);
              $semana=array();
              $i--;
          }else {
            array_push($semana,$i);
          }
        }
        $q=1;
        while(sizeOf($semana)<7){
          array_push($semana,$q);
          $q++;
        }
        array_push($mes,$semana);

        return $this->render('calculadoraBundle:Default:index.html.twig',array('mes' => $mes));
    }
}
